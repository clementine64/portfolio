import _ from 'lodash';
//import './second.scss';
import './style.scss';
//import $ from 'jquery';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';

$(document).ready(function(){
    $('.projets li').click(function(){
        var id = $(this).attr('id');
        $('.explication').hide();
        $('.texte_'+id).show();

        $('.details_projets').slideUp();
        $('#'+id+'_projets').show();

        $(this).addClass('active');
        $('.projets li').not(this).removeClass('active');
    });

    $('label').click(function(){
        $(this).toggleClass('checked');
        if($(this).hasClass('checked')){
            $('input[type=submit]').addClass('allowed');
        } else {
            $('input[type=submit]').removeClass('allowed');
        }
    });
});
/**
* Smooth scroll pour les ancres
**/
function scrollTo( target ) {
    if( target.length ) {
        $("html, body").stop().animate( { scrollTop: target.offset().top - 80 }, 1500);
    }
}
$('.navigation a').click(function(){
    var href = $(this).attr('href');
    scrollTo( $(href) );
});

$('form').submit(function(e){
    e.preventDefault();
    var datas = $(this).serialize();
    if($('#agree').prop('checked')){
        $.post(
            'traitement.php',
            {
                'datas': datas,
            },
            function(response){
                if(response){
                    $('.succes').text('Merci, j\'ai bien reçu votre email et reviendrai vers vous très bientôt !');
                    $('.succes').addClass('reussi');
                    $('.succes').removeClass('fail');
                } else {
                    $('.succes').text('Une erreur s\'est produite, veuillez recommencer.');
                    $('.succes').addClass('fail');
                    $('.succes').removeClass('reussi');
                }
            }
        );
    } else {
        $('.succes').text('Vous devez d\'abord accepter les conditions.');
        $('.succes').addClass('sndcolor');
        $('.succes').removeClass('reussi');
        $('.succes').removeClass('fail');
    }
});