<?php
    $datas = array();
    parse_str($_POST['datas'], $datas);

    $to = "c.fernandez@lilo.org";
    $subject = htmlspecialchars('Contact site Portfolio');

    $message = '
     Nom : '.htmlspecialchars($datas['nom']).'<br>
     Prénom : '.htmlspecialchars($datas['prenom']).'<br>
     Email : '.htmlspecialchars($datas['email']).'<br>
     Tel : '.htmlspecialchars($datas['tel']).'<br>
     Message : '.htmlspecialchars($datas['message']).'<br>
    ';

     $headers[] = 'MIME-Version: 1.0';
     $headers[] = 'Content-type: text/html; charset=utf-8';
     $headers[] = 'To: Clémentine Fernandez <c.fernandez@lilo.org>';
     $headers[] = 'From: '.$datas['nom'].' '.$datas['prenom'].' <'.$datas['email'].'>';

     // Envoi
     $mail = mail($to, $subject, $message, implode("\r\n", $headers));
     echo $mail;
?>