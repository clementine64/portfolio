<section id="parcours" class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-1 col-lg-7">
                <h2>Mon parcours</h2>
                <p class="item">
                    <strong class="thirdcolor">Sportive de haut-niveau</strong><br>
                    Escrimeuse depuis 22 ans, j'ai suivi le parcours haut-niveau et atteint la 3e place mondiale junior en 2010.<br>Membre du groupe France à l'INSEP chez les séniors, j'ai mis fin à cette aventure en 2015.<br>
                    <span class="maincolor">Mon gain en expérience :</span> dépassement de soi, confiance, remise en question et passion.
                </p>
                <p class="item">
                    <strong class="thirdcolor">Chargée de communication</strong><br>
                    Sortie des études, je m'oriente vers un service civique en communication à l'Agence pour l'Education par le Sport. Cette expérience me mène vers divers projets dans le milieu associatif et me fait intégrer un organisme de formation en tant que chargée de communication (Bleu Social).<br>
                    <span class="maincolor">Mon gain en expérience :</span> culture communication, milieu associatif, premières expériences avec le web.
                </p>
                <p class="item">
                    <strong class="thirdcolor">Intégratrice / Développeuse Web chez Welko Communication</strong><br>
                    Après avoir pris confiance dans le milieu du web, j'intègre l'équipe développement de Welko. L'Agence propose une diversité de projets web me permettant de m'épanouir dans ce domaine, et attisant ma curiosité pour ce vaste domaine !<br>
                    <span class="maincolor">Mon gain en expérience :</span> maîtrise de WordPress, process de développement, autonomie.
                </p>
            </div>
            <div class="col-xl-5 offset-xl-1 col-lg-5">
                <img src="img/course.svg" alt="Parcours" class="d-none d-lg-block">
                <h2 class="mt-5">Mes études</h2>
                <ul class="item">
                    <li>Formation Développeur Web - OpenClassRoom</li>
                    <li>Bac+5 Journalisme - ESJ Paris</li>
                    <li>Licence Etudes Européennes - Paris Sorbonne Nouvelle</li>
                </ul>
            </div>
        </div>
    </div>
</section>