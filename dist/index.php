<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Portfolio Clémentine Fernandez</title>
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <?php include('header.php');?>
    <section id="top" class="pt-5 pb-lg-5 pb-2 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <h2 class="sndcolor my-0">Clémentine Fernandez</h3>
                    <h1 class="mt-3">Développeuse web<br>Full Stack</h1>
                    <ul class="tags">
                        <li>HTML5</li>
                        <li>CSS / SASS</li>
                        <li>WordPress</li>
                        <li>Javascript</li>
                        <li>jQuery</li>
                        <li>Bootstrap</li>
                        <li>Symfony 4/5</li>
                        <li>Doctrine</li>
                        <li>API Rest</li>
                        <li>SQL</li>
                        <li>Git</li>
                        <li>GitLab</li>
                        <li>Webpack</li>
                        <li>AJAX</li>
                    </ul>
                </div>
                <div class="col-lg-7 d-none d-lg-block">
                    <?php include('img/webdeveloper.svg');?>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio" class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 offset-xl-1 col-lg-4">
                    <h2 class="text-right afterRight">Projets & réalisations</h2>
                    <ul class="projets">
                        <li id="salarie" class="active"><span>Salariée</span></li>
                        <li id="freelance"><span>Freelance</span></li>
                    </ul>
                    <div class="texte_salarie explication mt-lg-5 active">
                        <p class="text-right">Ces projets sont réalisés dans le cadre de mon travail chez Welko Communication. Le plus souvent, je suis intégrée à l'équipe : chef de projet / graphiste / intégration / développement.<br>Pour la plupart des projets, l'équipe développement est en lien direct avec le client.</p>
                    </div>
                    <div class="texte_freelance explication mt-lg-5">
                        <p class="text-right">Ces projets sont réalisés dans le cadre d'une activité freelance, bénévole ou expérimentale. Pour tout ces projets, aucun travail de graphiste n'a été commandé.</p>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-7">
                    <!-- SALARIE -->
                    <?php include('salarie.php');?>

                    <!-- FREEELANCE -->
                    <?php include('freelance.php');?>
                </div>
            </div>
        </div>
        <div class="element">
            <?php include('img/blooming.svg');?>
        </div>
    </section>
    <?php include('parcours.php');?>
    <?php include('formulaire.php');?>
    <?php include('footer.php');?>
    <script src="main.js"></script>
</body>
</html>