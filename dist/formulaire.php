<section id="contact" class="py-sm-5 py-3">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-7 mx-auto">
                <h2>Contactez-moi</h2>
                <p class="text-right mb-0"><a href="mailto:c.fernandez@lilo.org">c.fernandez@lilo.org</a></p>
                <p class="text-right mt-0">06 89 12 43 62</p>
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" placeholder="Votre nom" id="nom" name="nom" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" placeholder="Votre prénom" id="prenom" name="prenom">
                        </div>
                        <div class="col-sm-6">
                            <input type="email" placeholder="Votre email"  id="email" name="email" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" placeholder="Votre téléphone" id="tel" name="tel">
                        </div>
                        <div class="col-sm-12">
                            <textarea name="message" id="message" placeholder="Votre message" required></textarea>
                            <input type="checkbox" id="agree" name="agree">
                            <label for="agree">En cochant cette case, j'accepte que ma demande soit traitée puis conservée dans le cadre imposé par la réglementation.</label>
                            <input type="submit" value="Envoyer">
                            <p class="succes"></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>