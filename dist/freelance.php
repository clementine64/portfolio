<div id="freelance_projets" class="details_projets">
    <div class="row mt-4 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">Académie Musicale Philippe Jaroussky</h4>
                <p class="spec sndcolor my-0">Application Web</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="https://inscriptions.academiejaroussky.org" target="_blank" class="image">
                <img src="img/ampj2.png" alt="Yakavan">
            </a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Application web pour candidatures à l'Académie</span>
            </p>
            <ul class="mt-0">
                <li>Gestion des candidatures</li>
                <li>Espace candidat (candidature en plusieurs étapes)</li>
                <li>Export excel des candidats</li>
                <li>Outil évolutif vers un outil de gestion global</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#Symfony5</li>
                <li>#Doctrine</li>
                <li>#Application</li>
                <li>#API SendinBlue</li>
            </ul>
        </div>
    </div>

    <div class="row my-5 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">ARPEP Pays de la Loire</h4>
                <p class="spec sndcolor my-0">Dev. site e-commerce</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="http://esat.arpep-pdl.fr/" target="_blank" class="image">
                <img src="img/esat.png" alt="ESAT Argerie">
            </a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Demande de devis et commandes pour l'ESAT de l'Argerie</span>
            </p>
            <ul class="mt-0">
                <li>Développement d'un e-commerce devis/commande</li>
                <li>Gestion d'un catalogue de produits</li>
                <li>Intégration du module de paiement Stripe</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#Symfony4</li>
                <li>#Stripe</li>
                <li>#Doctrine</li>
                <li>#e-commerce</li>
            </ul>
        </div>
    </div>

    <div class="row my-5 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">Angers NDC Escrime</h4>
                <p class="spec sndcolor my-0">Dev. et Intégration</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="https://angersndc.org" target="_blank" class="image">
                <img src="img/angersndc.png" alt="Angers NDC">
            </a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Site vitrine du club d'escrime Angers NDC</span>
            </p>
            <ul class="mt-0">
                <li>Intégration et développement sur WordPress</li>
                <li>Utilisation de Webpack</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#WordPress</li>
                <li>#Webpack</li>
            </ul>
        </div>
    </div>
</div>