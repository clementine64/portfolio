<footer>
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-3">
                <h3 class="sndcolor">< cf <span class="maincolor">/></span></h3>
            </div>
            <div class="col-sm-6 col-9 d-flex justify-content-end">
                <p>Illustrations : <a target="_blank" href="https://undraw.co">undraw.co</a></p>
            </div> 
        </div>
    </div>
</footer>