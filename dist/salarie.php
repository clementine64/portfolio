<div id="salarie_projets" class="details_projets active">
    <div class="row mt-4 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">Yakavan</h4>
                <p class="spec sndcolor my-0">Dev. Site e-commerce</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="https://yakavan.re" target="_blank" class="image">
                <img src="img/yakavan.png" alt="Yakavan">
            </a>
            <a href="https://welko.fr"><small>Réalisation Welko - Agence de communication</small></a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Réservation de roadbooks et location de vans sur l'île de la Réunion.</span><br><br>
                Ma participation au projet :
            </p>
            <ul class="mt-0">
                <li>Développement wordpress</li>
                <li>Développement du plugin de réservation</li>
                <li>Intégration du module de paiement</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#Wordpress</li>
                <li>#FullCalendar.io</li>
            </ul>
        </div>
    </div>

    <div class="row my-5 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">Correggio Consulting</h4>
                <p class="spec sndcolor my-0">Dev. et Intégration</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="https://correggioconsulting.com" target="_blank" class="image">
                <img src="img/cc.png" alt="Correggio Consulting">
            </a>
            <a href="https://welko.fr"><small>Réalisation Welko - Agence de communication</small></a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Site d'offre d'emploi pour les cadres du secteur social & médicosocial</span><br><br>
                Les spécificités du projet :
            </p>
            <ul class="mt-0">
                <li>Lecture intelligente de cv pdf (extraction des nom/email)</li>
                <li>Candidature vidéo directement sur le site</li>
                <li>Développement d'un backoffice sur-mesure</li>
                <li>Outil de gestion des factures pour le client</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#mpdf</li>
                <li>#PdfToText</li>
                <li>#Composer</li>
                <li>#Wordpress</li>
            </ul>
        </div>
    </div>

    <div class="row my-5 single_projet">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <h4 class="sndcolor my-0">Académie Musicale Philippe Jaroussky</h4>
                <p class="spec sndcolor my-0">Dev. et Intégration</p>
            </div>
            <hr>
        </div>
        <div class="col-md-5">
            <a href="https://academiejaroussky.org" target="_blank" class="image">
                <img src="img/ampj.png" alt="Académie Jaroussky">
            </a>
            <a href="https://welko.fr"><small>Réalisation Welko - Agence de communication</small></a>
        </div>
        <div class="col-md-7">
            <p class="mb-0">
                <span class="sndcolor">Site vitrine de l'Académie Musicale Philippe Jaroussky</span><br><br>
                Les spécificités du projet :
            </p>
            <ul class="mt-0">
                <li>Intégration et développement des modules réutilisables</li>
                <li>Développement du calendrier des événements</li>
            </ul>
        </div>
        <div class="col-12">
            <ul class="tags">
                <li>#WordPress</li>
                <li>#ACFPro</li>
                <li>#FullCalendar</li>
            </ul>
        </div>
    </div>
</div>